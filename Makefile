# set target and source variables

bin_PROGRAMS = ckctl

ckctl_SOURCES = cmd/ckctl/ckctl.go
ckctl_DEPENDENCIES = libs

ifeq ($(target_architectures),)
	target_architectures := linux/amd64 linux/arm64 darwin/amd64 darwin/arm64
endif


# set go env variables

ifeq ($(CGO_ENABLED),)
	CGO_ENABLED = 0
endif

ifeq ($(GOPATH),)
	GOPATH := $(HOME)/go
endif

ifeq ($(GOBIN),)
	GOBIN := $(GOPATH)/bin
endif

ifeq ($(GOOS),)
	GOOS = linux
endif

ifeq ($(GOARCH),)
	GOARCH = amd64
endif


# set compiler variables and flags

ifeq ($(cc),)
	cc = go
endif

ifeq ($(CCLD),)
	CCLD = $(cc) build
endif

ifeq ($(version),)
	version = "$(shell git rev-parse HEAD)-dev"
endif

ifeq ($(ldflags),)
	ldflags = -w -s
	ldflags += -X "gitlab.com/m4573rh4ck3r/ckctl/internal/cmd.version=$(version)"
endif

ifeq ($(installflags),)
	installflags = -s
	installflags += -D
endif

ifeq ($(verbose),)
	verbose = true
endif

ifeq ($(buildflags),)
ifeq ($(verbose),true)
	buildflags = -x
endif
endif

ifeq ($(installflags),)
ifeq ($(verbose),true)
	installflags = -v
endif
endif

ifeq ($(LiNK),)
	LINK = CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) $(CCLD) $(buildflags) -ldflags "$(ldflags)"
endif


# set directory variables

srcdir := $(CURDIR)

ifeq ($(prefix),)
	prefix = /usr
endif

ifeq ($(bindir),)
	bindir := bin
endif

ifeq ($(builddir),)
	builddir = bin
endif

ifeq ($(confdir),)
	confdir := $(prefix)/etc/$(bin_PROGRAMS)
endif


# static targets

all: build

build: $(builddir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), $(LINK) -o "$(builddir)/$(bin_PROGRAM)" "$($(bin_PROGRAM)_SOURCES)";)

dist: $(builddir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), GOOS=$(GOOS) GOARCH=$(GOARCH) $(LINK) -o "$(builddir)/$(bin_PROGRAM)-$(GOOS)-$(GOARCH)" "$($(bin_PROGRAM)_SOURCES)";)

dist-all: $(builddir)
	@$(foreach target_arch,$(target_architectures), GOOS=$(shell echo $(target_arch) | sed 's/\/.*//g' ) GOARCH=$(shell echo $(target_arch) | sed 's/^.*\///g') $(MAKE) dist;)

libs:
	@GODEBUG=installgoroot=all go install std

clean:
	@rm -rvf "$(builddir)"
	@$(cc) clean -cache

install:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), install $(installflags) "$(builddir)/$(bin_PROGRAM)" "$(prefix)/$(bindir)";)

uninstall:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), rm -rvf "$(prefix)/$(bindir)/$(bin_PROGRAM)";)

upgrade-deps:
	@$(cc) get -u ./...

tidy:
	@$(cc) tidy

test:
	@true

.PHONY: all build clean dist install libs test tidy uninstall upgrade-deps


# dynamic targets

$(builddir):
	@mkdir -p $@

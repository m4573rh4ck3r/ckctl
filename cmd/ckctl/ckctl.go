package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"time"

	"gitlab.com/m4573rh4ck3r/ckctl/internal/cmd"
)

var (
	traceEnabled = os.Getenv("CKCTL_TRACE") == "enable"
)

func main() {
	if traceEnabled {
		f, err := os.Create(fmt.Sprintf("/tmp/ckctl_trace_%s.out", time.Now().Format("15_04_05_00")))
		if err != nil {
			panic(err)
		}
		defer func() {
			if err := f.Close(); err != nil {
				panic(err)
			}
		}()
		if err := trace.Start(f); err != nil {
			panic(err)
		}
		defer trace.Stop()
	}

	defer trace.Stop()
	cmd.Execute()
}

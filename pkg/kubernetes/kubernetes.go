package kubernetes

import (
	"context"
	"fmt"
	"log/slog"
	"sync"

	"cloud.google.com/go/container/apiv1/containerpb"

	"gitlab.com/m4573rh4ck3r/ckctl/pkg/kubernetes/api"
)

func RunOnAllClusters(ctx context.Context, projectId string, clusters []*containerpb.Cluster, args []string, command string, namespace string) error {
	var wg sync.WaitGroup

	for _, cluster := range clusters {
		wg.Add(1)
		go func(projectId string, cluster *containerpb.Cluster) {
			defer wg.Done()
			handler, err := api.NewHandler(ctx, projectId, cluster)
			if err != nil {
				slog.Error("failed to create handler", slog.String("error", err.Error()))
			}

			switch command {
			case "get":
				if err := handler.Get(ctx, args, namespace); err != nil {
					slog.Error(fmt.Sprintf("failed to get %s", args[0]), slog.String("project", projectId), slog.String("cluster", cluster.Name), slog.String("error", err.Error()))
				}
			}
		}(projectId, cluster)
	}

	wg.Wait()
	return nil
}

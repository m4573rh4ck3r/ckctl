package kubernetes

type Resource struct {
	Name    string
	Group   string
	Version string
	Verbs   []string
}

package kubernetes

import (
	"encoding/base64"
	"fmt"
	"log/slog"
	"os"
	"regexp"

	"cloud.google.com/go/container/apiv1/containerpb"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"
)

func GenerateKubeconfig(gcpClusters []*containerpb.Cluster) error {
	opts := clientcmd.NewDefaultPathOptions()

	var data []byte
	var err error

	data, err = os.ReadFile(opts.GetDefaultFilename())
	if err != nil {
		if !os.IsNotExist(err) {
			return fmt.Errorf("failed to read file %s: %v", opts.GetDefaultFilename(), err)
		}
	}
	if os.IsNotExist(err) {
		if err := os.WriteFile(opts.GetDefaultFilename(), data, 0600); err != nil {
			return fmt.Errorf("failed to create kubeconfig file: %v", err)
		}
	}

	if err := os.WriteFile(opts.GetDefaultFilename()+".bk", data, 0600); err != nil {
		return fmt.Errorf("failed to create backup of current kubeconfig file: %v", err)
	}

	currentConfig, err := clientcmd.LoadFromFile(opts.GetDefaultFilename())
	if err != nil {
		return err
	}

	for _, gcpCluster := range gcpClusters {
		matches := regexp.MustCompile(`projects\/([a-zA-Z0-9-]+)\/`).FindStringSubmatch(gcpCluster.SelfLink)
		if len(matches) == 0 {
			return fmt.Errorf("failed to extract GCP project ID in: %s", gcpCluster.SelfLink)
		}
		projectId := matches[len(matches)-1]
		name := fmt.Sprintf("gke_%s_%s_%s", projectId, gcpCluster.Location, gcpCluster.Name)
		cluster := api.NewCluster()
		cluster.Server = "https://" + gcpCluster.GetEndpoint()
		caCert, err := base64.StdEncoding.DecodeString(gcpCluster.MasterAuth.GetClusterCaCertificate())
		if err != nil {
			return err
		}
		cluster.CertificateAuthorityData = []byte(caCert)

		context := api.NewContext()
		context.Cluster = name
		context.AuthInfo = name

		user := api.NewAuthInfo()
		execConfig := api.ExecConfig{
			APIVersion:         "client.authentication.k8s.io/v1beta1",
			Command:            "gke-gcloud-auth-plugin",
			ProvideClusterInfo: true,
			InstallHint:        "Install gke-gcloud-auth-plugin for use with kubectl by following https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke",
		}
		user.Exec = &execConfig

		currentConfig.Clusters[name] = cluster
		currentConfig.Contexts[name] = context
		currentConfig.AuthInfos[name] = user
	}

	clientcmd.NewDefaultPathOptions()

	if err := clientcmd.WriteToFile(*currentConfig, opts.GetDefaultFilename()); err != nil {
		slog.Error("failed to write to kubeconfig file", slog.String("error", err.Error()))
	}

	slog.Info(fmt.Sprintf("successfully wrote kubeconfig to %s", opts.GetDefaultFilename()))

	return nil
}

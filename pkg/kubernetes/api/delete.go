package api

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/restmapper"
)

func (h *Handler) Delete(ctx context.Context, args []string, namespace string, filename string, dryRun bool) error {
	deleteOptions := metav1.DeleteOptions{}
	if dryRun {
		deleteOptions.DryRun = []string{"All"}
	}

	if filename != "" {
		f, err := os.Open(filename)
		if err != nil {
			return err
		}

		yamlReader := yaml.NewYAMLReader(bufio.NewReader(f))

		for {
			buf, err := yamlReader.Read()
			if err != nil {
				if err == io.EOF {
					break
				}
				return err
			}
			decode := scheme.Codecs.UniversalDeserializer().Decode
			obj, _, err := decode(buf, nil, nil)
			if err != nil {
				return err
			}

			unst := unstructured.Unstructured{}

			j, err := json.Marshal(obj)
			if err != nil {
				return err
			}

			if err := unst.UnmarshalJSON(j); err != nil {
				return err
			}

			gvr, err := h.getGroupVersionResourceForKind(obj.GetObjectKind().GroupVersionKind().Kind)
			if err != nil {
				return err
			}

			if err := h.DynamicClient.Resource(*gvr).Namespace(unst.GetNamespace()).Delete(ctx, unst.GetName(), deleteOptions); err != nil {
				return err
			}
		}

		return nil

	}

	lists, err := h.getResources(ctx, args[0], namespace)
	if err != nil {
		return fmt.Errorf("failed to get resources: %v", err)
	}

	for _, list := range lists {
		if list == nil {
			continue
		}

		groupResources, err := restmapper.GetAPIGroupResources(h.DiscoveryClient)
		if err != nil {
			return fmt.Errorf("failed to get group resources: %v", err)
		}
		restMapper := restmapper.NewDiscoveryRESTMapper(groupResources)
		if err != nil {
			return fmt.Errorf("failed to get dynamic rest mapper: %v", err)
		}

		if len(list.Items) == 0 {
			if strings.ToLower(namespace) == "all" {
				slog.Info("no resources found.", slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))
			} else {
				slog.Info(fmt.Sprintf("no resources found in %s namespace.", namespace), slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))
			}
		}

		for _, item := range list.Items {
			if item.GroupVersionKind().GroupKind().Kind == "PodMetrics" {
				continue
			}
			resMap, err := restMapper.RESTMapping(item.GroupVersionKind().GroupKind(), item.GetAPIVersion())
			if err != nil {
				return fmt.Errorf("failed to get rest mapping for %s: %v", item.GetName(), err)
			}
			res := schema.GroupVersionResource{
				Version: resMap.Resource.Version,
			}
			_, l, err := h.DiscoveryClient.ServerGroupsAndResources()
			if err != nil {
				return fmt.Errorf("failed to create discovery client: %v", err)
			}

			hasKind := false

			for _, apiResourceList := range l {
				for _, apiResource := range apiResourceList.APIResources {
					validNames := apiResource.ShortNames
					if apiResource.SingularName != "" { // TODO: sometimes this seems to be empty
						validNames = append(validNames, apiResource.SingularName)
					}
					validNames = append(validNames, apiResource.Name)

					for _, validName := range validNames {
						if validName == strings.ToLower(args[0]) {
							hasKind = true
							res.Resource = apiResource.Name
							break
						}
					}
					if hasKind {
						break
					}
				}
				if hasKind {
					break
				}
			}

			if !hasKind {
				return fmt.Errorf("the server does not have a resource named %s", args[0])
			}

			if resMap.Resource.Group != "" {
				res.Group = resMap.Resource.Group
			}

			if err := h.DynamicClient.Resource(res).Namespace(namespace).Delete(ctx, item.GetName(), deleteOptions); err != nil {
				return fmt.Errorf("failed to delete %s %s.", res.Resource, item.GetName())
			}
			if !dryRun {
				slog.Info(fmt.Sprintf("successfully deleted %s %s.", res.Resource, item.GetName()), slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))
			}
		}
	}

	return nil
}

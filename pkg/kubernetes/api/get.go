package api

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"k8s.io/cli-runtime/pkg/printers"
)

func (h *Handler) Get(ctx context.Context, args []string, namespace string) error {
	lists, err := h.getResources(ctx, args[0], namespace)
	if err != nil {
		return fmt.Errorf("failed to get resources: %v", err)
	}

	for i, list := range lists {
		if list == nil {
			continue
		}

		if len(list.Items) == 0 {
			if strings.ToLower(namespace) == "all" {
				slog.Info("no resources found.", slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))
			} else {
				slog.Info(fmt.Sprintf("no resources found in %s namespace.", namespace), slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))
			}
			if i+1 != len(lists) || len(lists) == 1 {
				fmt.Println()
			}
			continue
		}

		l := make(map[string]string)

		for _, item := range list.Items {
			for k, v := range item.GetLabels() {
				l[k] = v
			}

			l["GCP_PROJECT_ID"] = h.ProjectId
			l["CLUSTER_NAME"] = h.Cluster.Name
			l["KIND"] = item.GetKind()
			item.SetLabels(l)
		}

		pr, ok := printers.NewTablePrinter(printers.PrintOptions{
			WithNamespace: true,
			ColumnLabels:  []string{"KIND", "GCP_PROJECT_ID", "CLUSTER_NAME"},
		}).(*printers.HumanReadablePrinter)
		if !ok {
			slog.Error("failed to convert table printer into a human readable printer")
		}

		pr.PrintObj(list, os.Stdout)
		if i+1 != len(lists) || len(lists) == 1 {
			fmt.Println()
		}
	}

	return nil
}

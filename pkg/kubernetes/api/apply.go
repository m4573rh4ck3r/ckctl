package api

import (
	"bufio"
	"context"
	"encoding/json"
	"io"
	"os"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/kubernetes/scheme"
)

func (h *Handler) Apply(ctx context.Context, filename string, dryRun bool) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}

	yamlReader := yaml.NewYAMLReader(bufio.NewReader(f))

	for {
		buf, err := yamlReader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		decode := scheme.Codecs.UniversalDeserializer().Decode
		obj, _, err := decode(buf, nil, nil)
		if err != nil {
			return err
		}

		unst := unstructured.Unstructured{}

		j, err := json.Marshal(obj)
		if err != nil {
			return err
		}

		if err := unst.UnmarshalJSON(j); err != nil {
			return err
		}

		applyOptions := v1.ApplyOptions{
			FieldManager: "ckctl",
		}

		if dryRun {
			applyOptions.DryRun = []string{"All"}
		}

		gvr, err := h.getGroupVersionResourceForKind(obj.GetObjectKind().GroupVersionKind().Kind)
		if err != nil {
			return err
		}

		if _, err := h.DynamicClient.Resource(*gvr).Namespace(unst.GetNamespace()).Apply(ctx, unst.GetName(), &unst, applyOptions); err != nil {
			return err
		}
	}

	return nil
}

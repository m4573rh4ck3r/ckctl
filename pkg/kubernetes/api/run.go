package api

import (
	"context"
	"fmt"
	"log/slog"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func (h *Handler) Run(ctx context.Context, dryRun, tty, rm bool, image, name, namespace string, command ...string) error {
	clientSet, err := kubernetes.NewForConfig(h.RestConfig)
	if err != nil {
		return err
	}

	pod := &v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{{
				Name:    name,
				Image:   image,
				Command: command,
				Stdin:   false,
				TTY:     tty,
			}},
		},
	}

	createOptions := metav1.CreateOptions{}

	if dryRun {
		createOptions.DryRun = []string{"All"}
	}

	if _, err := clientSet.CoreV1().Pods(namespace).Create(ctx, pod, createOptions); err != nil {
		return err
	}

	if dryRun {
		return nil
	}

	deleteOptions := metav1.DeleteOptions{}
	if err := clientSet.CoreV1().Pods(namespace).Delete(ctx, pod.Name, deleteOptions); err != nil {
		return err
	}
	slog.Info(fmt.Sprintf("successfully deleted pod %s", pod.Name), slog.String("project", h.ProjectId), slog.String("cluster", h.Cluster.Name))

	return nil
}

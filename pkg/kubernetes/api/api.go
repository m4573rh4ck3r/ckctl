package api

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"strings"

	"cloud.google.com/go/container/apiv1/containerpb"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"
)

type Handler struct {
	ProjectId       string
	Cluster         *containerpb.Cluster
	RestConfig      *rest.Config
	DynamicClient   dynamic.Interface
	DiscoveryClient *discovery.DiscoveryClient
}

func NewHandler(ctx context.Context, projectId string, cluster *containerpb.Cluster) (*Handler, error) {
	restConfig, err := getRestConfig(ctx, projectId, cluster)
	if err != nil {
		return nil, err
	}

	dynamicClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	return &Handler{
		ProjectId:       projectId,
		RestConfig:      restConfig,
		DynamicClient:   dynamicClient,
		DiscoveryClient: discoveryClient,
		Cluster:         cluster,
	}, nil
}

func getRestConfig(ctx context.Context, projectId string, cluster *containerpb.Cluster) (*rest.Config, error) {
	kubeConfig := &api.Config{
		APIVersion: "v1",
		Clusters:   map[string]*api.Cluster{},
		AuthInfos:  map[string]*api.AuthInfo{},
		Contexts:   map[string]*api.Context{},
	}
	name := fmt.Sprintf("gke_%s_%s_%s", projectId, cluster.Location, cluster.Name)
	masterAuth := cluster.GetMasterAuth()

	cert, err := base64.StdEncoding.DecodeString(masterAuth.ClusterCaCertificate)
	if err != nil {
		return nil, fmt.Errorf("failed to parse cluster ca certificate: %v", err)
	}

	kubeConfig.Clusters[name] = &api.Cluster{
		CertificateAuthorityData: cert,
		Server:                   "https://" + cluster.Endpoint,
	}
	kubeConfig.Contexts[name] = &api.Context{
		Cluster:  name,
		AuthInfo: name,
	}

	kubeConfig.AuthInfos[name] = &api.AuthInfo{
		Exec: &api.ExecConfig{
			APIVersion:         "client.authentication.k8s.io/v1beta1",
			Command:            "gke-gcloud-auth-plugin",
			ProvideClusterInfo: true,
			InteractiveMode:    "IfAvailable",
		},
	}

	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()

	configOverrides := &clientcmd.ConfigOverrides{}

	reader := new(io.Reader)
	clientConfig := clientcmd.NewInteractiveClientConfig(*kubeConfig, name, configOverrides, *reader, loadingRules)

	return clientConfig.ClientConfig()
}

func (h *Handler) getResources(ctx context.Context, resourceName, namespace string) ([]*unstructured.UnstructuredList, error) {
	_, l, err := h.DiscoveryClient.ServerGroupsAndResources()
	if err != nil {
		return nil, fmt.Errorf("failed to create discovery client: %v", err)
	}

	var resources []*schema.GroupVersionResource

	hasKind := false

	for _, apiResourceList := range l {
		for _, apiResource := range apiResourceList.APIResources {
			validNames := apiResource.ShortNames
			if apiResource.SingularName != "" { // TODO: sometimes this seems to be empty
				validNames = append(validNames, apiResource.SingularName)
			}
			validNames = append(validNames, apiResource.Name)

			validForAll := false

			for _, category := range apiResource.Categories {
				if category == "all" {
					validForAll = true
				}
			}

			for _, validName := range validNames {
				if validName == strings.ToLower(resourceName) || (strings.ToLower(resourceName) == "all" && validForAll) { // TODO: pods and podmetrics have the same name 'pods' and only the Kind differs.
					hasKind = true
					res := schema.GroupVersionResource{}
					res.Resource = apiResource.Name
					gv, err := schema.ParseGroupVersion(apiResourceList.GroupVersion)
					if err != nil {
						return nil, fmt.Errorf("failed to parse groupVersion: %v", err)
					}
					res.Version = gv.Version
					if gv.Group != "" {
						res.Group = gv.Group
					}
					resources = append(resources, &res)
					break
				}
			}
			if hasKind {
				break
			}
		}
		if hasKind {
			break
		}
	}

	if !hasKind {
		return nil, fmt.Errorf("the server does not have a resource named %s", resourceName)
	}

	var ns string

	switch namespace {
	case "":
		ns = corev1.NamespaceDefault
	case "default":
		ns = corev1.NamespaceDefault
	case "all":
		ns = corev1.NamespaceAll
	default:
		ns = namespace
	}

	var retResources []*unstructured.UnstructuredList

	for _, res := range resources {
		r, err := h.DynamicClient.Resource(*res).Namespace(ns).List(ctx, metav1.ListOptions{})
		if err != nil {
			return nil, fmt.Errorf("failed to list resources: %v", err)
		}
		retResources = append(retResources, r)
	}

	return retResources, nil
}

func (h *Handler) getGroupVersionResourceForKind(kind string) (*schema.GroupVersionResource, error) {
	gvr := &schema.GroupVersionResource{}

	_, l, err := h.DiscoveryClient.ServerGroupsAndResources()
	if err != nil {
		return gvr, fmt.Errorf("failed to create discovery client: %v", err)
	}

	for _, apiResourceList := range l {
		for _, apiResource := range apiResourceList.APIResources {
			if apiResource.Kind == kind { // TODO: pods and podmetrics have the same name 'pods' and only the Kind differs.
				gv, err := schema.ParseGroupVersion(apiResourceList.GroupVersion)
				if err != nil {
					return nil, err
				}
				gvr.Resource = apiResource.Name
				gvr.Version = gv.Version
				if gv.Group != "" {
					gvr.Group = gv.Group
				}

				return gvr, nil
			}
		}
	}

	return nil, fmt.Errorf("the server does not have a kind named %s", kind)
}

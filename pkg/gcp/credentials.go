package gcp

import (
	"context"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"
)

func GetOptions(ctx context.Context) (*option.ClientOption, error) {
	token := os.Getenv("GOOGLE_OAUTH_ACCESS_TOKEN")
	if token != "" {
		tokenSource := oauth2.StaticTokenSource(&oauth2.Token{
			AccessToken: token,
		})
		opts := option.WithTokenSource(tokenSource)
		return &opts, nil
	}

	jsonFile := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	if jsonFile != "" {
		opts := option.WithCredentialsFile(jsonFile)
		return &opts, nil
	}

	creds, err := google.FindDefaultCredentials(ctx, compute.CloudPlatformScope)
	if err != nil {
		return nil, err
	}

	opts := option.WithCredentials(creds)
	return &opts, nil
}

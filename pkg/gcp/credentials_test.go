package gcp

import (
	"context"
	"testing"
)

func TestGetOptions(t *testing.T) {
	ctx := context.Background()
	_, err := GetOptions(ctx)
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkGetOptions(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ctx := context.Background()
		_, err := GetOptions(ctx)
		if err != nil {
			b.Fatal(err)
		}
	}
}

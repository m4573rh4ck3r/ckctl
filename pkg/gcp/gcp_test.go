package gcp

import (
	"context"
	"testing"

	compute "cloud.google.com/go/compute/apiv1"
)

func TestGetZones(t *testing.T) {
	ctx := context.Background()
	opts, err := GetOptions(ctx)
	if err != nil {
		panic(err)
	}
	projects, err := GetProjects(ctx, opts, "*")
	if err != nil {
		panic(err)
	}
	if len(projects) == 0 {
		panic("could not find any gcp projects")
	}

	zonesClient, err := compute.NewZonesRESTClient(ctx, *opts)
	if err != nil {
		t.Fatal(err)
	}

	zones, err := getZones(ctx, projects[0].Name, "name = europe-west*", zonesClient)
	if err != nil {
		t.Fatal(err)
	}
	if len(zones) == 0 {
		t.Fatal("received empty list of zones")
	}
}

func BenchmarkGetZones(b *testing.B) {
	ctx := context.Background()
	opts, err := GetOptions(ctx)
	if err != nil {
		panic(err)
	}

	projects, err := GetProjects(ctx, opts, "*")
	if err != nil {
		panic(err)
	}
	if len(projects) == 0 {
		panic("could not find any gcp projects")
	}

	zonesClient, err := compute.NewZonesRESTClient(ctx, *opts)
	if err != nil {
		b.Fatal(err)
	}

	for i := 0; i < b.N; i++ {
		zones, err := getZones(ctx, projects[0].Name, "name = europe-west*", zonesClient)
		if err != nil {
			b.Fatal(err)
		}
		if len(zones) == 0 {
			b.Fatal("received empty list of zones")
		}
	}
}

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	resourcemanager "cloud.google.com/go/resourcemanager/apiv3"
	resourcemanagerpb "cloud.google.com/go/resourcemanager/apiv3/resourcemanagerpb"

	compute "cloud.google.com/go/compute/apiv1"
	"cloud.google.com/go/compute/apiv1/computepb"
	container "cloud.google.com/go/container/apiv1"
	"cloud.google.com/go/container/apiv1/containerpb"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

var (
	parallel int
)

func GetProjects(ctx context.Context, opts *option.ClientOption, projectRegex string) ([]*resourcemanagerpb.Project, error) {
	var projects []*resourcemanagerpb.Project

	resourcemanagerClient, err := resourcemanager.NewProjectsClient(ctx)
	if err != nil {
		return nil, err
	}

	projectsSearchQuery := fmt.Sprintf("name:%s", projectRegex)
	searchProjectsRequest := &resourcemanagerpb.SearchProjectsRequest{
		Query: projectsSearchQuery,
	}

	it := resourcemanagerClient.SearchProjects(ctx, searchProjectsRequest)
	for {
		projectsSearchResp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		projects = append(projects, projectsSearchResp)
	}

	// resp, err := resourcemanagerClient.ListProjects(ctx)
	// if err != nil {
	// 	return nil, err
	// }

	// var results []*resourcemanagerpb.Project
	// if projectRegex != "" {
	// 	for _, project := range resp.Projects {
	// 		if regexp.MustCompile(projectRegex).MatchString(project.ProjectId) {
	// 			results = append(results, project)
	// 		}
	// 	}
	// } else {
	// 	results = resp.Projects
	// }

	return projects, nil
}

func GetClusters(ctx context.Context, projectId string, locationFilter string, opts *option.ClientOption) ([]*containerpb.Cluster, error) {
	var locations []string

	regionsClient, err := compute.NewRegionsRESTClient(ctx, *opts)
	if err != nil {
		return nil, err
	}

	regions, err := getRegions(ctx, projectId, locationFilter, regionsClient)
	if err != nil {
		return nil, err
	}
	locations = append(locations, regions...)

	zonesClient, err := compute.NewZonesRESTClient(ctx, *opts)
	if err != nil {
		return nil, err
	}
	zones, err := getZones(ctx, projectId, locationFilter, zonesClient)
	if err != nil {
		return nil, err
	}
	locations = append(locations, zones...)

	containerClient, err := container.NewClusterManagerClient(ctx, *opts)
	if err != nil {
		return nil, err
	}
	defer containerClient.Close()

	var clusters []*containerpb.Cluster
	for _, location := range locations {
		req := &containerpb.ListClustersRequest{
			Parent: fmt.Sprintf("projects/%s/locations/%s", projectId, location),
		}

		clusterListResp, err := containerClient.ListClusters(ctx, req)
		if err != nil {
			return nil, err
		}
		clusters = append(clusters, clusterListResp.Clusters...)
	}
	if len(clusters) == 0 {
		slog.Info(
			"no clusters found",
			slog.String("project", projectId),
		)
		return nil, nil
	}

	return clusters, nil
}

func getRegions(ctx context.Context, projectId string, locationFilter string, regionsClient *compute.RegionsClient) ([]string, error) {
	var regions []string
	regionsFilter := fmt.Sprintf("name = %s", locationFilter)
	listRegionsRequest := &computepb.ListRegionsRequest{
		Filter:  &regionsFilter,
		Project: projectId,
	}

	it := regionsClient.List(ctx, listRegionsRequest)
	for {
		regionsListResp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		regions = append(regions, *regionsListResp.Name)
	}

	return regions, nil
}

func getZones(ctx context.Context, projectId string, locationFilter string, zonesClient *compute.ZonesClient) ([]string, error) {
	var zones []string
	zoneFilter := fmt.Sprintf("name = %s", locationFilter)
	listZonesRequest := &computepb.ListZonesRequest{
		Filter:  &zoneFilter,
		Project: projectId,
	}

	it := zonesClient.List(ctx, listZonesRequest)
	for {
		zonesListResp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		zones = append(zones, *zonesListResp.Name)
	}

	return zones, nil
}

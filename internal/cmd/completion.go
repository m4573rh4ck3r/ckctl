package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
)

var completionCmd = &cobra.Command{
	Use:       "completion",
	Short:     "Print shell completions",
	Args:      cobra.ExactArgs(1),
	ValidArgs: []string{"bash", "zsh", "fish"},
	Run: func(cmd *cobra.Command, args []string) {
		switch args[0] {
		case "bash":
			if err := rootCmd.GenBashCompletionV2(os.Stdout, false); err != nil {
				slog.Error("failed to generate bash completion: %v", err)
			}
		case "zsh":
			if err := rootCmd.GenZshCompletion(os.Stdout); err != nil {
				slog.Error("failed to generate zsh completion: %v", err)
			}
		case "fish":
			if err := rootCmd.GenFishCompletion(os.Stdout, true); err != nil {
				slog.Error("failed to generate fish completion: %v", err)
			}
		}
	},
}

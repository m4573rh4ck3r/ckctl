package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"regexp"
	"strings"
	"sync"

	"cloud.google.com/go/container/apiv1/containerpb"
	"cloud.google.com/go/resourcemanager/apiv3/resourcemanagerpb"
	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/ckctl/pkg/gcp"
	"gitlab.com/m4573rh4ck3r/ckctl/pkg/kubernetes/api"
)

func init() {
	deleteCmd.Flags().StringVarP(&filename, "filename", "f", "", "name of the file containing the configuration to delete")
	deleteCmd.MarkFlagFilename("filename", "yaml", "yml")
}

var deleteCmd = &cobra.Command{
	Use:  "delete",
	Args: cobra.MaximumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 && filename == "" {
			slog.Error("please provide either a filename using the -f oder --filename flag or a resource followed by it's name using the positional arguments")
			os.Exit(1)
		}

		sem := make(chan struct{}, parralel)
		defer close(sem)

		ctx := context.Background()
		var wg sync.WaitGroup

		opts, err := gcp.GetOptions(ctx)
		if err != nil {
			slog.Error("failed to get gcp credentials", slog.String("error", err.Error()))
			os.Exit(1)
		}

		slog.Debug("found credentials")

		projects, err := gcp.GetProjects(ctx, opts, projectsRegex)
		if err != nil {
			slog.Error("failed to retrieve gcp projects", slog.String("error", err.Error()))
			os.Exit(1)
		}
		slog.Debug("received list of gcp projects")

		projectsChan := make(chan *resourcemanagerpb.Project, len(projects))

		wg.Add(1)
		go func() {
			defer wg.Done()
			for _, project := range projects {
				projectsChan <- project
			}
			close(projectsChan)
		}()

		clustersChan := make(chan []*containerpb.Cluster, len(projects))

		wg.Add(1)
		go func() {
			defer wg.Done()
			var pwg sync.WaitGroup
			pwg.Add(len(projects))

			for range projects {
				go func() {
					defer pwg.Done()
					project, ok := <-projectsChan
					if !ok {
						slog.Error("projects channel was closed unexpectedly")
						os.Exit(1)
					}

					clusters, err := gcp.GetClusters(ctx, project.ProjectId, locationsRegex, opts)
					clustersChan <- clusters
					if err != nil {
						slog.Error("failed to retrieve list of clusters", slog.String("project", project.ProjectId), slog.String("error", err.Error()))
						return
					}

					return
				}()
			}
			pwg.Wait()
			close(clustersChan)
		}()

		for range projects {
			sem <- struct{}{}
			wg.Add(1)
			go func() {
				defer wg.Done()
				defer func() {
					<-sem
				}()

				clusters, ok := <-clustersChan
				if !ok {
					slog.Error("cluster channel was closed unexpectedly")
					os.Exit(1)
				}

				for _, cluster := range clusters {
					projectId := strings.Replace(regexp.MustCompile(`^https\:\/\/container\.googleapis\.com\/v1\/projects\/[a-zA-Z0-9-_]+`).FindString(cluster.SelfLink), "https://container.googleapis.com/v1/projects/", "", 1)
					h, err := api.NewHandler(ctx, projectId, cluster)
					if err != nil {
						slog.Error("failed to get handler", slog.String("cluster", cluster.SelfLink), slog.String("error", err.Error()))
					}

					if err := h.Delete(ctx, args, namespace, filename, dryRun); err != nil {
						slog.Error(fmt.Sprintf("%v", err))
					}
				}
			}()
		}

		wg.Wait()
	},
}

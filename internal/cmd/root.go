package cmd

import (
	"context"
	"log/slog"
	"os"
	"runtime"
	"time"

	"github.com/spf13/cobra"
)

var (
	projectsRegex  string
	locationsRegex string
	namespace      string
	parralel       int
	verbose        int
	dryRun         bool
	ctx            context.Context
)

const (
	colorReset      = "\033[0m"
	colorFaint      = "\033[2m"
	colorResetFaint = "\033[22m"
	colorRed        = "\033[31m"
	colorYellow     = "\033[33m"
	colorCyan       = "\033[36m"
)

var rootCmd = &cobra.Command{
	Use: "ckctl",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		logLevel := slog.Level(verbose)
		handler := slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: logLevel,
			ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == slog.TimeKey {
					a.Value = slog.StringValue(time.Now().Format("15:04:05.00"))
				}

				// TODO: fix escaping
				// 				if a.Key == slog.LevelKey {
				// 					level := a.Value.Any().(slog.Level)
				// 					switch {
				// 					case level == slog.LevelDebug:
				// 						a.Value = slog.StringValue(colorFaint + "DEBUG" + colorResetFaint)
				// 					case level == slog.LevelInfo:
				// 						a.Value = slog.StringValue(colorCyan + "INFO" + colorReset)
				// 					case level == slog.LevelWarn:
				// 						a.Value = slog.StringValue(colorYellow + "WARN" + colorReset)
				// 					case level == slog.LevelError:
				// 						a.Value = slog.StringValue(colorRed + "ERROR" + colorReset)
				// 					}
				// 				}

				return a
			},
		})

		slog.SetDefault(slog.New(handler))
	},
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&namespace, "namespace", "n", "default", "kubernetes namespace")
	rootCmd.PersistentFlags().StringVarP(&projectsRegex, "projects", "p", "*", "regex to select GCP projects")
	rootCmd.PersistentFlags().StringVarP(&locationsRegex, "locations", "l", "europe-west1", "regex to select GCP locations (zones or regions)")
	rootCmd.PersistentFlags().IntVar(&parralel, "parallel", runtime.GOMAXPROCS(0), "maximum number of concurrent goroutines")
	rootCmd.PersistentFlags().IntVarP(&verbose, "verbose", "v", 0, "be more verbose. Error: 0, Warn: 4, Info: 0, Debug: -4")
	rootCmd.PersistentFlags().BoolVar(&dryRun, "dry-run", false, "activate dry run")

	rootCmd.AddCommand(
		applyCmd,
		getCmd,
		deleteCmd,
		completionCmd,
		generateKubeconfigCmd,
		runCmd,
		versionCmd,
	)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		slog.Error(err.Error())
	}
}

package cmd

import (
	"context"
	"log/slog"
	"os"
	"sync"

	"cloud.google.com/go/container/apiv1/containerpb"
	"cloud.google.com/go/resourcemanager/apiv3/resourcemanagerpb"

	"gitlab.com/m4573rh4ck3r/ckctl/pkg/gcp"
	"gitlab.com/m4573rh4ck3r/ckctl/pkg/kubernetes"

	"github.com/spf13/cobra"
)

var generateKubeconfigCmd = &cobra.Command{
	Use:   "generate-kubeconfig",
	Short: "append all clusters to the kubeconfig file",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()
		var mu sync.Mutex
		var allClusters []*containerpb.Cluster
		var wg sync.WaitGroup

		opts, err := gcp.GetOptions(ctx)
		if err != nil {
			slog.Error("failed to get gcp credentials", slog.String("error", err.Error()))
			os.Exit(1)
		}

		slog.Debug("found credentials")

		projects, err := gcp.GetProjects(ctx, opts, projectsRegex)
		if err != nil {
			slog.Error("failed to retrieve gcp projects", slog.String("error", err.Error()))
			os.Exit(1)
		}
		slog.Debug("received list of gcp projects")

		projectsChan := make(chan *resourcemanagerpb.Project, len(projects))

		wg.Add(1)
		go func() {
			defer wg.Done()

			for _, project := range projects {
				projectsChan <- project
			}

			close(projectsChan)
		}()

		for range projects {
			wg.Add(1)
			go func() {
				defer wg.Done()
				project, ok := <-projectsChan
				if !ok {
					slog.Error("projects channel was closed unexpectedly")
					os.Exit(1)
				}

				clusters, err := gcp.GetClusters(ctx, project.ProjectId, locationsRegex, opts)
				if err != nil {
					slog.Error("failed to retrieve list of clusters", slog.String("project", project.ProjectId), slog.String("error", err.Error()))
					return
				}

				mu.Lock()
				allClusters = append(allClusters, clusters...)
				mu.Unlock()
			}()
		}

		wg.Wait()

		if err := kubernetes.GenerateKubeconfig(allClusters); err != nil {
			slog.Error("failed to generate kubeconfig", slog.String("error", err.Error()))
			os.Exit(1)
		}
	},
}
